defmodule Bloggie.Repo do
  use Ecto.Repo,
    otp_app: :bloggie,
    adapter: Ecto.Adapters.Postgres
end
